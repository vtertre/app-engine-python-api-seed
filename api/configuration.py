# -*- coding: utf-8 -*-

http_configuration = {
    u'number_of_retries': 5,
    u'maximum_retry_delay_in_seconds': 32
}
