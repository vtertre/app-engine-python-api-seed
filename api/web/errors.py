# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod

from injector import Key, inject
from werkzeug.exceptions import HTTPException

from api.command.validators import ValidationError

ErrorResolvers = Key(u'error_resolvers')


class ApplicationStatusService(object):
    @inject(resolvers=ErrorResolvers)
    def __init__(self, resolvers):
        self.resolvers = resolvers

    def handle(self, error):
        resolver = self._resolver(error)
        status = resolver.status(error) if resolver else 500
        representation = resolver.representation(error) if resolver else None
        return resolver is not None, status, representation

    def _resolver(self, error):
        resolvers = filter(lambda resolver: resolver.can_resolve(error), self.resolvers)
        return resolvers[0] if len(resolvers) > 0 else None


class ErrorResolver(object):
    __metaclass__ = ABCMeta

    def can_resolve(self, error):
        return error is not None and isinstance(error, self.error_type)

    @property
    @abstractmethod
    def error_type(self):
        raise NotImplementedError

    @abstractmethod
    def status(self, error=None):
        raise NotImplementedError

    @abstractmethod
    def representation(self, error):
        return None


class ValidationErrorResolver(ErrorResolver):
    @property
    def error_type(self):
        return ValidationError

    def status(self, error=None):
        return 400

    def representation(self, error):
        messages_representation = [{u'message': message} for message in error.messages]
        return {u'errors': messages_representation}


class FlaskHTTPErrorResolver(ErrorResolver):
    @property
    def error_type(self):
        return HTTPException

    def status(self, error=None):
        return error.code if error else 500

    def representation(self, error):
        return {u'errors': [{u'message': error.description}]}
