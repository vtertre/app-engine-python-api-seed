# -*- coding: utf-8 -*-
import logging
from os import getenv

from flask_injector import FlaskInjector

from api.web.server import Server
from api.web.your_application import YourApplication

level = logging.INFO if getenv(u'ENV', u'development') == u'production' else logging.DEBUG
formatter = logging.Formatter(getenv(u'LOGGING_PATTERN'), u'%(message)s')
root_logger = logging.getLogger()
handler = root_logger.handlers[0] if root_logger.handlers else logging.StreamHandler()
handler.setFormatter(formatter)
handler.setLevel(level)
root_logger.addHandler(handler)
root_logger.setLevel(level)

application = YourApplication()
server = Server(application)

FlaskInjector(app=server.flask, injector=application.injector)
