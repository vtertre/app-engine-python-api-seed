#!/usr/bin/env bash

set -e

if [ -z $1 ] || [ -z $2 ] ; then
	echo "Usage: $0 <version> <gcloud account>"
	exit 1
fi

echo "==== Configuring dependencies for production ===="

if [ ! -d "venv" ]; then
    virtualenv venv
    source venv/bin/activate
fi

rm -rf libs
venv/bin/pip2 install -qr requirements.txt -t libs/

echo "==== Configuration app for production ===="

perl -pi -e "s/'development'/'staging'/g" api.yaml

echo "==== Deploying app to App Engine ===="

gcloud app deploy \
    api.yaml \
    --account=$2 \
    --project=TODO \
    --version=$1 \
    --quiet

perl -pi -e "s/'staging'/'development'/g" api.yaml

venv/bin/pip2 install -qr dev_requirements.txt -t libs/ --upgrade
